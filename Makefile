###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author EmilyPham <emilyn3@hawaii.edu>
# @brief  Lab 06b - Pointers - EE 205 - Spr 2021
# @date   16_MAR_2021
###############################################################################

pointers: pointers.cpp
	g++ -o pointers pointers.cpp
	
